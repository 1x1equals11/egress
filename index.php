<?php
  require 'database/db_connect.php';
  session_start();
  
  $error = 3; //default
  $error_msg = "";

  if (isset($_POST['login'])) {
    $query1 = "SELECT password,full_name,id FROM account WHERE user_name = '$_POST[user_name]'";
    $to_unpack = mysqli_query($con,$query1);
    $fetch_account = mysqli_fetch_assoc($to_unpack);
    $hashed_password = $fetch_account['password'];

    if (password_verify($_POST['password'],$hashed_password)) {
      $error = 0;
      $_SESSION['user_id'] = $fetch_account['id'];
      $_SESSION['full_name'] = $fetch_account['full_name'];

      echo "<script>
              setTimeout(function(){
                window.location.href = 'admin/';
              }, 4000);
            </script>";
    }else{
      $error = 1;
      $error_msg = "Something went wrong please contact system administrator.";
    }
  }
 ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Login</title>

    <!-- Main styles for this application-->
    <link href="coreui/css/coreui2.css" rel="stylesheet">

  </head>

  <body class="c-app c-dark-theme flex-row align-items-center">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-8">
          <span style="display:<?php echo ($error == 1 ? 'block' : 'none' ); ?>">
            <div class="alert alert-danger" role="alert">
              <?php echo $error_msg; ?>
            </div>
          </span>

          <span style="display:<?php echo ($error == 0 ? 'block' : 'none' ); ?>">
            <div class="alert alert-success" role="alert">
              Success! You will be redirected to dashboard.
            </div>
          </span>
          <div class="card-group">
            <div class="card p-4">
              <!-- was-validated -->
              <div class="card-body ">

                <h1>Login</h1>
                <p class="text-muted">Sign in to your account</p>

                <form method="post">
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <svg class="c-icon">
                          <use xlink:href="coreui/sprites/free.svg#cil-user"></use>
                        </svg>
                      </span>
                    </div>
                    <input class="form-control" type="text" name="user_name"placeholder="Username">
                  </div>

                  <div class="input-group mb-4">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <svg class="c-icon">
                          <use xlink:href="coreui/sprites/free.svg#cil-lock-locked"></use>
                        </svg>
                      </span>
                    </div>
                    <input class="form-control" type="password" name="password" placeholder="Password">
                  </div>

                  <div class="row">
                    <div class="col-6">
                      <button type="submit" class="btn btn-primary px-4" name="login" type="button">Login</button>
                    </div>
                  </div>

                </form>
              </div>
            </div>
            <div class="card text-white bg-light py-5 d-md-down-none" style="width:44%">
              <div class="card-body text-center">
                <div>
                  <h2>Egress  </h2>
                  <p>Be part of our growing company. Fill in the application form by clicking the button below</p>
                  <a class="btn btn-lg btn-outline-info mt-3"href='register.php'>Create an account</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- CoreUI and necessary plugins-->
    <script src="coreui/js/coreui.bundle.min.js"></script>

  </body>
</html>
