<?php
  require '../database/db_connect.php';
  session_start();

  $error = 3; //default
  $error_msg = "";
  $success_msg = "";

  if (!isset($_SESSION['full_name'])) {
    header("location:/ble/");
  }

  if (isset($_POST['step2'])) {
    $query1 = "SELECT MAX(id) AS MAX_ID FROM locations WHERE account_id = '$_SESSION[user_id]'";
    $result = mysqli_query($con,$query1);
    $max_id = mysqli_fetch_assoc($result)['MAX_ID'];

    $original_file_name = $_FILES['floor_img']['name'];
    $tmp_file_destination = $_FILES['floor_img']['tmp_name'];
    $file_error = $_FILES['floor_img']['error'];
    $file_size = $_FILES['floor_img']['size'];
    $exploded_file_name = explode(".",$original_file_name);
    $file_extension = strtolower(end($exploded_file_name));
    $allowed_extensions = array('jpg','JPG', 'JPEG', 'png', 'PNG');

    if ($file_error!=0) {
      $error = 1;
      $error_msg = "Failed upon uploading file";
    }

    if (!in_array($file_extension,$allowed_extensions)) {
      $error = 1;
      $error_msg = "Invalid file type";
    }

    if ($file_size > 10000000) {
      $error = 1;
      $error_msg = "File is too big";
    }

    $_FILES['floor_img']['unique_file_name'] = uniqid("file-",true).".".$file_extension;
    $file_destination = "../uploads/".$_FILES['floor_img']['unique_file_name'];
    $tmp_file_destination = $_FILES['floor_img']['tmp_name'];
    $unique_file_name = $_FILES['floor_img']['unique_file_name'];
    $query2 = "INSERT INTO floors
              (location_id, name, floor_number, floor_img) VALUES
              ('$max_id', '$_POST[name]', '$_POST[floor_number]', '$unique_file_name')";

    if (move_uploaded_file($tmp_file_destination, $file_destination)) {
      if (mysqli_query($con,$query2)) {
        $error = 0;
        $success_msg = "Successfully added floor plan. You may now plot your desired beacon positions";
      }else{
        $error_msg = "Something went wrong. Please contact system administrator.";
      }
    }

  }


 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <title>Add Floor Plan</title>

    <!-- Main styles for this application-->
    <link href="../coreui/css/style.css" rel="stylesheet">
    <!-- <link rel="stylesheet" href="../print.css"> -->
  </head>
  <body class="c-app c-dark-theme flex-row align-items-center" id='body'>
    <!-- c-sidebar-lg-show -->
    <div class="c-sidebar c-sidebar-dark c-sidebar-fixed " id="sidebar">
      <div class="c-sidebar-brand d-md-down-none">
        <img class="c-sidebar-brand-full" width="118" height="46" src="../bahagia/bahagia2.png" alt="">
        <!-- <svg class="c-sidebar-brand-full" width="118" height="46" alt="CoreUI Logo" style="fill:currentColor;">
          <use xlink:href="../coreui/sprites/brand.svg#cib-coreui"></use>
        </svg> -->
        <img class="c-sidebar-brand-minimized" width="38" height="46" src="../bahagia/mini_logo.png" alt="">
        <!-- <svg class="c-sidebar-brand-minimized" width="46" height="46" alt="CoreUI Logo">
          <use xlink:href="../coreui/sprites/brand.svg#cib-coreui"></use>
        </svg> -->
      </div>

      <ul class="c-sidebar-nav ps ps--active-y">

      <li class="c-sidebar-nav-item active">
        <a class="c-sidebar-nav-link" href="index.php">
          <svg class="c-sidebar-nav-icon">
            <use xlink:href="../coreui/sprites/free.svg#cil-home"></use>
          </svg> My Locations
        </a>
      </li>

      <li class="c-sidebar-nav-title">Options</li>

      <li class="c-sidebar-nav-item">
        <a class="c-sidebar-nav-link" href="create_location.php">
          <svg class="c-sidebar-nav-icon">
            <use xlink:href="../coreui/sprites/free.svg#cil-location-pin"></use>
          </svg> Create New Location
        </a>
      </li>


      <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
        <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
      </div>

      <div class="ps__rail-y" style="top: 0px; height: 551px; right: 0px;">
        <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 234px;"></div>
      </div>
    </ul>

    <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent" data-class="c-sidebar-unfoldable"></button>

  </div>

  <div class="c-wrapper">
    <header class="c-header c-header-light c-header-fixed">

      <button class="c-header-toggler c-class-toggler d-lg-none mfe-auto" type="button" data-target="#sidebar" data-class="c-sidebar-show">
        <svg class="c-icon c-icon-lg">

          <use xlink:href="../coreui/sprites/free.svg#cil-menu"></use>
        </svg>
      </button>

      <a class="c-header-brand d-lg-none c-header-brand-sm-up-center" href="#">
        <img id='toggle_brand' class="c-sidebar-brand-full" width="118" height="46" src="../bahagia/bahagia2.png" >
        <!-- <svg width="118" height="46" alt="CoreUI Logo" style="fill:currentColor;">
          <use xlink:href="../coreui/sprites/brand.svg#cib-coreui"></use>
        </svg> -->
      </a>

      <button class="c-header-toggler c-class-toggler mfs-3 d-md-down-none" type="button" data-target="#sidebar" data-class="c-sidebar-lg-show" responsive="true">
        <svg class="c-icon c-icon-lg">
          <use xlink:href="../coreui/sprites/free.svg#cil-menu"></use>
        </svg>
      </button>

      <ul class="c-header-nav mfs-auto">
        <li class="c-header-nav-item px-3 c-d-legacy-none">
          <button class="c-header-nav-btn" type="button" id="header-tooltip" title="Toggle Light/Dark Mode">
            <svg class="c-icon c-d-dark-none">
              <use xlink:href="../coreui/sprites/free.svg#cil-moon"></use>
            </svg>

            <svg class="c-icon c-d-default-none">
              <use xlink:href="../coreui/sprites/free.svg#cil-sun"></use>
            </svg>
          </button>

        </li>
      </ul>
      <div class="d-none d-sm-inline-flex c-header-nav mt-1">
        <h6><?php echo $_SESSION['full_name']; ?></h6>
      </div>
      <ul class="c-header-nav mr-2">
        <li class="c-header-nav-item dropdown">
          <a class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            <div class="c-avatar">
              <img class="c-avatar-img" src="../assets/img/avatars/male_default.jpg" alt="user@email.com">
            </div>
          </a>

          <div class="dropdown-menu dropdown-menu-right pt-0">

          <div class="dropdown-header bg-light py-2"><strong>Settings</strong></div>

            <a class="dropdown-item" href="../logout.php">
              <svg class="c-icon mfe-2">
                <use xlink:href="../coreui/sprites/free.svg#cil-account-logout"></use>
              </svg> Logout
            </a>
          </div>
        </li>
      </ul>

      <!-- <div class="c-subheader justify-content-between px-3">

        <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
          <li class="breadcrumb-item"> <a href="index.php" style="color:grey;">Dashboard</a> </li>

        </ol>

      </div> -->
    </header>
    <div class="c-body">
      <main class="c-main">
        <div class="container-fluid">
          <div class="fade-in">
            <div class="row">
              <div class="col-12">

                <span style="<?php echo ($error != 3 ? "visibility:visible;" : "visibility:hidden;"); ?>">
                  <div class="alert <?php echo ( !empty($error_msg) ? "alert-danger": "alert-success" ); ?> p-2 m-0 mb-1 text-center" role="alert">
                    <?php echo ( !empty($error_msg) ? $error_msg : $success_msg ); ?>
                  </div>
                </span>

                <script type="text/javascript">
                  var error = <?php echo $error; ?>;

                  if (error != 3) {
                    setTimeout(function(){
                      window.location.href = 'add_floorplan.php';
                    }, 3700);
                  }
                </script>

              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="card">

                  <div class="card-header">
                    <div class="row">
                      <h6 class="col-10 col-xl-11">Create New Location</h6>
                    </div>
                  </div>

                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-12">

                        <form method="post" enctype="multipart/form-data">
                          <div class="input-group mb-3">
                            <input class="form-control" type="text" name="name" placeholder="Name *"> <br>
                          </div>

                          <div class="input-group mb-2">
                            &nbsp&nbsp <small>  E.g. "SM Lanang First Floor"</small>
                          </div>

                          <div class="input-group mb-3">
                            <input class="form-control" type="number" name="floor_number" placeholder="Floor number *">
                          </div>

                          <div class="input-group mb-3">
                            <label class="input-group-text custom-file-label" id='file_label'>Upload floor plan *</label>
                            <input class="form-control custom-file-input" id='post_file' name='floor_img' type="file" onchange="loadFile(event)">
                          </div>

                          <div class="form-group">
                            <div id="thumbnail" style="display:none;">
                              <img class="img-fluid" id="img_thumbnail" src="" alt="thumb" width="300px">
                            </div>
                          </div>

                          <script type="text/javascript">

                            var loadFile = function(event) {
                              var file_name = document.getElementById('post_file').value;
                              var split = file_name.split('\\');
                              document.getElementById('file_label').innerHTML = split[split.length-1];
                              document.getElementById('thumbnail').style.display = 'block';
                              var output = document.getElementById('img_thumbnail');
                              output.src = URL.createObjectURL(event.target.files[0]);
                              output.onload = function() {
                                URL.revokeObjectURL(output.src)
                              }
                            };
                            //
                            // function redirect(id) {
                            //   window.location.href='profile.php?id='+id;
                            // }
                          </script>
                          <div class="input-group mb-3">
                            <input class="btn btn-info"type="submit" name="step2" value="Save Changes">
                          </div>

                        </form>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
    <footer class="c-footer">
      <div><a href="https://coreui.io">CoreUI</a> © 2020 creativeLabs.</div>
      <div class="mfs-auto">Powered by&nbsp;<a href="https://coreui.io/pro/">CoreUI Pro</a></div>
    </footer>
  </div>
  <!-- CoreUI and necessary plugins-->
  <script src="../coreui/js/jquery.min.js"></script>
  <script src="../coreui/js/coreui.bundle.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function () {
        $('#header-tooltip').on("click",function () {
          var state = $("#body").attr("class");
          var classes = state.split(" ");
          var isDark = false;

          for (var i = 0; i < classes.length; i++) {
            if (classes[i] == "c-dark-theme") {
              isDark = true;
            }
          }

          if (isDark) {
            $('#body').attr("class","c-app flex-row align-items-center");
            $('#toggle_brand').attr("src","../bahagia/bahagia1.png");
            $('#print-btn').css("color","black");
          }else{
            $('#body').attr("class","c-app flex-row align-items-center c-dark-theme");
            $('#toggle_brand').attr("src","../bahagia/bahagia2.png");
            $('#print-btn').css("color","white");
          }
        });
    });
  </script>
  </body>
</html>
