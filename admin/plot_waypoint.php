<?php
  require '../database/db_connect.php';
  session_start();

  $error = 3; //default
  $error_msg = "";
  $success_msg = "";
  $floor_id = $_GET['floor_id'];
  $query = "SELECT * FROM floors WHERE id = '$floor_id'";
  $query2 = "SELECT * FROM beacons WHERE floor_id = '$floor_id'";

  $to_unpack = mysqli_query($con,$query);
  $fetch_floor = mysqli_fetch_assoc($to_unpack);

  $to_unpack = mysqli_query($con,$query2);
  $beacon_list = array();

  while ($beacon = mysqli_fetch_assoc($to_unpack)) {
    if ($beacon['beacon_type'] != 'EXIT') {
      array_push($beacon_list,$beacon);
    }
  }

  // if (!isset($_SESSION['full_name'])) {
  //   header("location:/ble/");
  // }

  if (isset($_POST['form_trigger'])) {
    unset($_POST['form_trigger']);
    // echo sizeof($_POST);
    $floor_id = $_GET['floor_id'];
    $mac_address = "";
    $x_coordinate = "";
    $y_coordinate = "";
    $beacon_type = "";
    $query = "INSERT INTO beacons
              (floor_id, mac_address, x_coordinate, y_coordinate, beacon_type) VALUES ";
    $rows = $_POST['row'];

    for ($i=0; $i < sizeof($rows); $i++) {
      $mac_address = $rows[$i]['mac_address'];
      $x_coordinate = $rows[$i]['x_coordinate'];
      $y_coordinate = $rows[$i]['y_coordinate'];
      $beacon_type = $rows[$i]['beacon_type'];
      $query.="('$floor_id','$mac_address','$x_coordinate','$y_coordinate','$beacon_type')";

      mysqli_query($con,$query);
      $query = "INSERT INTO beacons
                (floor_id, mac_address, x_coordinate, y_coordinate, beacon_type) VALUES ";
    }

    echo ((mysqli_error($con))? mysqli_error($con) : "Success");
  }

 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <title>Plot Waypoints</title>

    <!-- Main styles for this application-->
    <link href="../coreui/css/style.css" rel="stylesheet">
    <!-- <link rel="stylesheet" href="../print.css"> -->
  </head>
  <body class="c-dark-theme align-items-center" id='body'>
    <div class="d-flex">
      <form method="post" id="canvas_form">
        <input id='form_trigger' type="hidden" name="form_trigger" value="">
        <img id="floor_plan" width="220" height="277" src="<?php echo "../uploads/".$fetch_floor['floor_img']; ?>" style="display:none;">
        <canvas id="myCanvas" width="600" height="400"
          style="border:1px solid #d3d3d3;">
        </canvas>

        <div class="d-flex">
          <!-- <button id="beacon_btn" class="btn btn-primary"type="button" name="button" data-toggle="modal" data-target="#exampleModalCenter">Add Beacon</button> -->
          <button id="beacon_btn" class="btn btn-primary"type="button" name="button">Add Waypoint</button>&nbsp;
          <input id="canvas_save"type="submit" class="btn btn-success" name="canvas_submit" value="Save Changes">
          <div id="xycoordinates" style="margin-left:230px;">
            Coordinates: (0,0)
          </div>
        </div>
      </form>

      <div class="card ml-4 mt-1" style="width:53%;height:400px;">
        <div class="card-header">
          <div class="row" style="align-items:center;">
            <h6 class="col text-center">ROUTE PLANNER</h6>
            <select class="col form-control" id="beacon_selector">
              <option hidden value='1'>Select Beacon</option>
              <?php
                for ($i=0; $i < sizeof($beacon_list); $i++) {
                  ?>
                  <option value="<?php echo $beacon_list[$i]['id']; ?>"><?php echo $beacon_list[$i]['mac_address']; ?></option>
                  <?php
                }
               ?>
            </select>

          </div>
        </div>

        <div class="card-body"style="overflow-y:auto;">
          <div class="row">
            <table class="table table-responsive-xl table-hover text-center">
              <thead class="thead-dark">
                <tr>
                  <th scope="col">Waypoint ID</th>
                  <th scope="col">Coordinates</th>
                  <th scope="col">Align To</th>
                  <!-- <th scope="col">Distance (meters)</th> -->
                  <th scope="col">Settings</th>
                </tr>
              </thead>
              <tbody id="table-body">

              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>

  <!-- CoreUI and necessary plugins-->
  <script src="../coreui/js/jquery.min.js"></script>
  <script src="../coreui/js/coreui.bundle.min.js"></script>
  <script type="text/javascript">
    var canvas = null;
    var ctx = null;
    var img = "";
    var state = "";
    var x = 0;
    var y = 0;
    var beacon_list = null;
    var current_beacon = null;
    var waypoint_list = null;

    $(document).ready(function () {

      initialize();

      function initialize() {
        // Create canvas with image
        canvas = document.getElementById("myCanvas");
        ctx = canvas.getContext("2d");
        img = document.getElementById("floor_plan");
        ctx.drawImage(img, 0, 0, 600, 400);

        // Coordinates properties
        state = "default";
        x = 0;
        y = 0;

        // Convert php beacons array to js array
        beacon_list = <?php echo json_encode($beacon_list); ?>;
        waypoint_list = [];

        // Disable add waypoint btn
        if ($("#beacon_selector").val() == '1') {
          $("#beacon_btn").attr("disabled",true);
        }else{
          $("#beacon_btn").attr("disabled",false);
        }

        // Clear table rows
        var check_rows = document.getElementsByClassName('table-row');
        var size = check_rows.length;
        if (check_rows.length != 0) {
          for (var i = 0; i < size; i++) {
            check_rows[0].remove();
          }
        }

        var check_align_to = document.getElementsByClassName('waypoint_select');
        size = check_align_to.length;
        if (check_align_to.length != 0) {
          for (var i = 0; i < size; i++) {
            check_align_to[0].remove();
          }
        }

        // check_align_to = document.getElementsByClassName('waypoint_select');
        // console.log(check_align_to);
      }

      $("#beacon_btn").on('click',function (event) {
        canvas.style.cursor='crosshair';
        state = "adding";
      });

      $("#beacon_selector").on("change", function (event) {
        initialize();
        var beacon_id_selected = $("#beacon_selector").val();

        for (var i = 0; i < beacon_list.length; i++) {
            if (beacon_id_selected == beacon_list[i]['id']) {
              current_beacon = beacon_list[i];
              break;
            }
        }

        x = current_beacon['x_coordinate'];
        y = current_beacon['y_coordinate'];

        $("#beacon_selected").attr("value",current_beacon['id']).html(current_beacon['mac_address']);

        plot();
      });

      $("#myCanvas").on("click", function (event) {
        if (state != "default") {
          // $("#modal_coordinate").html(x + "," + y);
          $("#clientX").val(x);
          $("#clientY").val(y);
          $("#exampleModalCenter").modal('show');
        }
        canvas.style.cursor='default';
        state = "default";
      });

      $("#myCanvas").on("mousemove", function (event) {
        x=event.clientX;
        y=event.clientY;
        document.getElementById("xycoordinates").innerHTML="Coordinates: (" + x + "," + y + ")";
      });

      $("#myCanvas").on("mouseout", function (event) {
        document.getElementById("xycoordinates").innerHTML="Coordinates: (0,0)";
      })

      $("#close1").on("click", function (event) {
        $("#exampleModalCenter").modal('hide');
        for (var i = 0; i < $(".modal-form").length-1; i++) {
          document.getElementsByClassName("modal-form")[i].value = "";
        }
      });

      $("#save_changes").on("click", function (event) {
        x = $("#clientX").val();
        y = $("#clientY").val();
        var inputs = document.getElementsByClassName("modal-form");
        var setting = "<td><div class='dropdown'><a class='btn btn-secondary dropdown-toggle' id='dropdownMenuLink' href='#' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Actions</a><div class='dropdown-menu' aria-labelledby='dropdownMenuLink'><input class='dropdown-item' type='submit' name='complete' value='Edit'><input class='dropdown-item' type='submit' name='complete' value='Remove'></div></div></td>";
        var to_append = $("<tr class='table-row'><td>"+inputs[2].value+"</td><td>"+x+","+y+"</td><td>"+inputs[3].value+"</td>"+setting+"</tr>");
        var align_to_x = 0;
        var align_to_y = 0;

        $("#exampleModalCenter").modal('hide');
        $("#table-body").append(to_append);

        //Populate waypoints array
        waypoint_list.push({
          waypoint_name : inputs[2].value,
          coordinates : x+","+y
        });

        // if (waypoint_list.length>0) {
          console.log("Debug: "+(waypoint_list.length - 1));
          to_append = $("<option class='waypoint_select' value='"+waypoint_list[waypoint_list.length-1]['waypoint_name']+"'>"+waypoint_list[waypoint_list.length-1]['waypoint_name']+"</option>");
          $("#align_to").append(to_append);
        // }
        // console.log(waypoint_list[0]);
        //Check for selected nodes
        // var check_selected_rows = document.getElementsByClassName('')
        // for (var i = 0; i < array.length; i++) {
        //   array[i]
        // }

        //Assign beacon coordinates if selected
        for (var i = 0; i < beacon_list.length; i++) {
          if (beacon_list[i]['id'] == inputs[3].value) {
            align_to_x = beacon_list[i]['x_coordinate'];
            align_to_y = beacon_list[i]['y_coordinate'];
            break;
          }
        }

        for (var i = 0; i < waypoint_list.length; i++) {
          var waypointXY = waypoint_list[i]['coordinates'].split(",");
          var waypoint_x = waypointXY[0];
          var waypoint_y = waypointXY[1];

          if (waypoint_list[i]['waypoint_name'] == inputs[3].value) {
            align_to_x = waypoint_x;
            align_to_y = waypoint_y;
            break;
          }
        }

        // //If beacons is not selected, then assign waypoint coords
        // align_to_x = ((align_to_x) ? align_to_x : "waypoint_x");
        // align_to_y = ((align_to_y) ? align_to_y : "waypoint_y");

        plot();
        drawLine(x,y,align_to_x,align_to_y);

        //Clear modal values
        var modal_form = document.getElementsByClassName("modal-form");
        for (var i = 0; i < modal_form.length; i++) {
          modal_form[i].value = "";
        }

      });

      function plot() {
        // Draw shits
        ctx.fillStyle = "red";
        ctx.beginPath();
        ctx.arc(x,y,5,0,2*Math.PI);
        ctx.closePath();
        ctx.fill();
      }

      function drawLine(fromx, fromy, tox, toy) {
        ctx.lineWidth = 10;
        ctx.strokeStyle = "red";
        ctx.beginPath();
        ctx.moveTo(fromx, fromy);
        ctx.lineTo(tox,toy);
        ctx.stroke();
      }

      $("#canvas_save").on("click",function (event) {
        event.preventDefault();
        var row = document.getElementsByClassName('table-row');
        var to_append = null;

        for (var i = 0; i < row.length; i++) {
          var curRow = row[i];
          var xy_split = curRow.childNodes[1].innerHTML.split(',');
          to_append = $("<input type='hidden' name=row["+i+"]["+"mac_address"+"] value='"+curRow.childNodes[0].innerHTML+"'>");
          $("#canvas_form").append(to_append);
          to_append = $("<input type='hidden' name=row["+i+"]["+"x_coordinate"+"] value='"+xy_split[0]+"'>");
          $("#canvas_form").append(to_append);
          to_append = $("<input type='hidden' name=row["+i+"]["+"y_coordinate"+"] value='"+xy_split[1]+"'>");
          $("#canvas_form").append(to_append);
          to_append = $("<input type='hidden' name=row["+i+"]["+"beacon_type"+"] value='"+curRow.childNodes[2].innerHTML+"'>");
          $("#canvas_form").append(to_append);
        }

        $("#form_trigger").val("1");
        $("#canvas_form").submit();
      });

      $('#header-tooltip').on("click",function () {
        var state = $("#body").attr("class");
        var classes = state.split(" ");
        var isDark = false;

        for (var i = 0; i < classes.length; i++) {
          if (classes[i] == "c-dark-theme") {
            isDark = true;
          }
        }

        if (isDark) {
          $('#body').attr("class","c-app flex-row align-items-center");
          $('#toggle_brand').attr("src","../bahagia/bahagia1.png");
          $('#print-btn').css("color","black");
        }else{
          $('#body').attr("class","c-app flex-row align-items-center c-dark-theme");
          $('#toggle_brand').attr("src","../bahagia/bahagia2.png");
          $('#print-btn').css("color","white");
        }

        $('#info-modal-close').on('click', function(event){
          window.location.href = 'index.php';
        });

        $('#close').on('click', function(event){
          window.location.href = 'index.php';
        });
      });
    });

  </script>

  <div class="modal fade" id="exampleModalCenter" tabindex="-1" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Waypoint Information</h5>
        <!-- <button class="btn-close" type="button" id="close1" data-coreui-dismiss="modal" aria-label="Close"></button> -->
      </div>
      <div class="modal-body">

        <div class="input-group">
          <label style="font-size:15px;">(X,Y):</label> &nbsp; &nbsp;
          <input class="form-control form-control-sm modal-form" id="clientX" type="text">-
          <input class="form-control form-control-sm modal-form" id="clientY" type="text">
          <!-- <div for=""> <label id="modal_coordinate"></label> </div> -->
        </div>
        <br>

        <div class="input-group">
            <input class="form-control modal-form" type="text" id="waypoint_name" placeholder="Waypoint Custom Name">
        </div>
        <br>

        <div class="input-group">
          <select class="form-control modal-form" id='align_to'>
            <option disabled hidden selected>Align to</option>
            <option id='beacon_selected' value=""></option>
          </select>
        </div>
        <br>

      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" id="close1" data-coreui-dismiss="modal">Cancel</button>
        <button class="btn btn-primary" type="button" id="save_changes">Save changes</button>
      </div>
    </div>
  </div>
</div>
  </body>

</html>
