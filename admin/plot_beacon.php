<?php
  require '../database/db_connect.php';
  session_start();

  $error = 3; //default
  $error_msg = "";
  $success_msg = "";
  $floor_id = $_GET['floor_id'];
  $query = "SELECT * FROM floors WHERE id = '$floor_id'";
  $to_unpack = mysqli_query($con,$query);
  $fetch_floor = mysqli_fetch_assoc($to_unpack);

  // if (!isset($_SESSION['full_name'])) {
  //   header("location:/ble/");
  // }

  if (isset($_POST['form_trigger'])) {
    unset($_POST['form_trigger']);
    // echo sizeof($_POST);
    $floor_id = $_GET['floor_id'];
    $mac_address = "";
    $x_coordinate = "";
    $y_coordinate = "";
    $beacon_type = "";
    $query = "INSERT INTO beacons
              (floor_id, mac_address, x_coordinate, y_coordinate, beacon_type) VALUES ";
    $rows = $_POST['row'];

    for ($i=0; $i < sizeof($rows); $i++) {
      $mac_address = $rows[$i]['mac_address'];
      $x_coordinate = $rows[$i]['x_coordinate'];
      $y_coordinate = $rows[$i]['y_coordinate'];
      $beacon_type = $rows[$i]['beacon_type'];
      $query.="('$floor_id','$mac_address','$x_coordinate','$y_coordinate','$beacon_type')";

      mysqli_query($con,$query);
      $query = "INSERT INTO beacons
                (floor_id, mac_address, x_coordinate, y_coordinate, beacon_type) VALUES ";
    }

    echo ((mysqli_error($con))? mysqli_error($con) : "Success");
  }

 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <title>Plot Beacon</title>

    <!-- Main styles for this application-->
    <link href="../coreui/css/style.css" rel="stylesheet">
    <!-- <link rel="stylesheet" href="../print.css"> -->
  </head>
  <body class="c-dark-theme align-items-center" id='body'>
    <div class="d-flex">
      <form method="post" id="canvas_form">
        <input id='form_trigger' type="hidden" name="form_trigger" value="">
        <img id="floor_plan" width="220" height="277" src="<?php echo "../uploads/".$fetch_floor['floor_img']; ?>" style="display:none;">
        <canvas id="myCanvas" width="600" height="400"
          style="border:1px solid #d3d3d3;">
        </canvas>

        <div class="d-flex">
          <!-- <button id="beacon_btn" class="btn btn-primary"type="button" name="button" data-toggle="modal" data-target="#exampleModalCenter">Add Beacon</button> -->
          <button id="beacon_btn" class="btn btn-primary"type="button" name="button">Add Beacon</button>&nbsp;
          <input id="canvas_save"type="submit" class="btn btn-success" name="canvas_submit" value="Save Changes">
          <div id="xycoordinates" style="margin-left:230px;">
            Coordinates: (0,0)
          </div>
        </div>
      </form>

      <div class="card ml-4 mt-1" style="width:50%;height:400px;">

        <div class="card-header">
          <div class="row">
            <h6 class="col-12 col-xl-11 text-center">Beacons Plotted</h6>
          </div>
        </div>

        <div class="card-body"style="overflow-y:auto;">
          <div class="row">
            <table class="table table-responsive-xl table-hover text-center">
              <thead class="thead-dark">
                <tr>
                  <th scope="col">Mac Address</th>
                  <th scope="col">Coordinates</th>
                  <th scope="col">Beacon Type</th>
                  <th scope="col">Settings</th>
                </tr>
              </thead>
              <tbody id="table-body">
                <!-- <tr>
                  <td>F9:40:36:DF:7C:DB</td>
                  <td>125,327</td>
                  <td>Standard</td>
                  <td>
                    <div class="dropdown">
                      <a class="btn btn-secondary dropdown-toggle" id="dropdownMenuLink" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Actions
                      </a>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <input class="dropdown-item" type="submit" name="complete" value="Remove">
                      </div>
                    </div>
                  </td>
                </tr> -->
              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>

    <!-- <ul class="list-group" >
      <li class="list-group-item">F9:40:36:DF:7C:DB</li>
      <li class="list-group-item">Z9:50:26:DF:7C:DB</li>
      <li class="list-group-item">H9:60:81:DF:7C:DB</li>
      <li class="list-group-item">R9:70:99:DF:7C:DB</li>
      <li class="list-group-item">S9:80:14:DF:7C:DB</li>
      <li class="list-group-item">F9:40:36:DF:7C:DB</li>
      <li class="list-group-item">Z9:50:26:DF:7C:DB</li>
      <li class="list-group-item">H9:60:81:DF:7C:DB</li>
      <li class="list-group-item">R9:70:99:DF:7C:DB</li>
      <li class="list-group-item">S9:80:14:DF:7C:DB</li>
    </ul> -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalCenterTitle">Beacon Information</h5>
          <!-- <button class="btn-close" type="button" id="close1" data-coreui-dismiss="modal" aria-label="Close"></button> -->
        </div>
        <div class="modal-body">
          <div class="input-group">
            <label style="font-size:15px;">(X,Y):</label> &nbsp; &nbsp;
            <input class="form-control form-control-sm modal-form" id="clientX" type="text">-
            <input class="form-control form-control-sm modal-form" id="clientY" type="text">
            <!-- <div for=""> <label id="modal_coordinate"></label> </div> -->
          </div>
          <br>
          <div class="input-group">
            <input class="form-control modal-form"type="text" id="mac_address" value="" placeholder="BLE Mac Address">
          </div>
          <br>
          <div class="input-group">
            <select class="form-control modal-form" name="">
              <option value="STANDARD">Standard Beacon</option>
              <option value="EXIT">Exit Beacon</option>
            </select>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" id="close1" data-coreui-dismiss="modal">Cancel</button>
          <button class="btn btn-primary" type="button" id="save_changes">Save changes</button>
        </div>
      </div>
    </div>
  </div>


  <!-- CoreUI and necessary plugins-->
  <script src="../coreui/js/jquery.min.js"></script>
  <script src="../coreui/js/coreui.bundle.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function () {
      // Initialize canvas
      var canvas = document.getElementById("myCanvas");
      var ctx = canvas.getContext("2d");
      var img = document.getElementById("floor_plan");
      ctx.drawImage(img, 0, 0, 600, 400);

      var bb = $("#beacon_btn");
      var state = "default";
      var x = 0;
      var y = 0;
      var table_row = 0;

      bb.on('click',function (event) {
        canvas.style.cursor='crosshair';
        state = "adding";
      });

      $("#myCanvas").on("click", function (event) {
        if (state != "default") {
          // $("#modal_coordinate").html(x + "," + y);
          $("#clientX").val(x);
          $("#clientY").val(y);
          $("#exampleModalCenter").modal('show');
        }
        canvas.style.cursor='default';
        state = "default";
      });

      $("#myCanvas").on("mousemove", function (event) {
        x=event.clientX;
        y=event.clientY;
        document.getElementById("xycoordinates").innerHTML="Coordinates: (" + x + "," + y + ")";
      });

      $("#myCanvas").on("mouseout", function (event) {
        document.getElementById("xycoordinates").innerHTML="Coordinates: (0,0)";
      })

      $("#close1").on("click", function (event) {
        $("#exampleModalCenter").modal('hide');
        for (var i = 0; i < $(".modal-form").length-1; i++) {
          document.getElementsByClassName("modal-form")[i].value = "";
        }
      });

      $("#save_changes").on("click", function (event) {
        x = $("#clientX").val();
        y = $("#clientY").val();
        var inputs = document.getElementsByClassName("modal-form");
        var setting = "<td><div class='dropdown'><a class='btn btn-secondary dropdown-toggle' id='dropdownMenuLink' href='#' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Actions</a><div class='dropdown-menu' aria-labelledby='dropdownMenuLink'><input class='dropdown-item' type='submit' name='complete' value='Edit'><input class='dropdown-item' type='submit' name='complete' value='Remove'></div></div></td>";
        var to_append = $("<tr class='table-row'><td>"+inputs[2].value+"</td><td>"+x+","+y+"</td><td>"+inputs[3].value+"</td>"+setting+"</tr>");

        $("#exampleModalCenter").modal('hide');
        $("#table-body").append(to_append);

        //Clear modal values
        for (var i = 0; i < $(".modal-form").length-1; i++) {
          document.getElementsByClassName("modal-form")[i].value = "";
        }

        // Draw shits
        ctx.fillStyle = "red";
        ctx.beginPath();
        ctx.arc(x,y,5,0,2*Math.PI);
        ctx.closePath();
        ctx.fill();
        // ctx.stroke();
      });

      $("#canvas_save").on("click",function (event) {
        event.preventDefault();
        var row = document.getElementsByClassName('table-row');
        var to_append = null;

        for (var i = 0; i < row.length; i++) {
          var curRow = row[i];
          var xy_split = curRow.childNodes[1].innerHTML.split(',');
          to_append = $("<input type='hidden' name=row["+i+"]["+"mac_address"+"] value='"+curRow.childNodes[0].innerHTML+"'>");
          $("#canvas_form").append(to_append);
          to_append = $("<input type='hidden' name=row["+i+"]["+"x_coordinate"+"] value='"+xy_split[0]+"'>");
          $("#canvas_form").append(to_append);
          to_append = $("<input type='hidden' name=row["+i+"]["+"y_coordinate"+"] value='"+xy_split[1]+"'>");
          $("#canvas_form").append(to_append);
          to_append = $("<input type='hidden' name=row["+i+"]["+"beacon_type"+"] value='"+curRow.childNodes[2].innerHTML+"'>");
          $("#canvas_form").append(to_append);
        }

        $("#form_trigger").val("1");
        $("#canvas_form").submit();
      });

      $('#header-tooltip').on("click",function () {
        var state = $("#body").attr("class");
        var classes = state.split(" ");
        var isDark = false;

        for (var i = 0; i < classes.length; i++) {
          if (classes[i] == "c-dark-theme") {
            isDark = true;
          }
        }

        if (isDark) {
          $('#body').attr("class","c-app flex-row align-items-center");
          $('#toggle_brand').attr("src","../bahagia/bahagia1.png");
          $('#print-btn').css("color","black");
        }else{
          $('#body').attr("class","c-app flex-row align-items-center c-dark-theme");
          $('#toggle_brand').attr("src","../bahagia/bahagia2.png");
          $('#print-btn').css("color","white");
        }

        $('#info-modal-close').on('click', function(event){
          window.location.href = 'index.php';
        });

        $('#close').on('click', function(event){
          window.location.href = 'index.php';
        });
      });
    });

  </script>
  </body>

</html>
