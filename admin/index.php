<?php
  require '../database/db_connect.php';
  session_start();


  if (!isset($_SESSION['full_name'])) {
    header("location:/ble/");
  }

 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <title>Admin Dashboard</title>

    <!-- Main styles for this application-->
    <link href="../coreui/css/style.css" rel="stylesheet">
    <!-- <link rel="stylesheet" href="../print.css"> -->
  </head>
  <body class="c-app c-dark-theme flex-row align-items-center" id='body'>
    <!-- c-sidebar-lg-show -->
    <div class="c-sidebar c-sidebar-dark c-sidebar-fixed " id="sidebar">
      <div class="c-sidebar-brand d-md-down-none">
        <img class="c-sidebar-brand-full" width="118" height="46" src="../bahagia/bahagia2.png" alt="">
        <!-- <svg class="c-sidebar-brand-full" width="118" height="46" alt="CoreUI Logo" style="fill:currentColor;">
          <use xlink:href="../coreui/sprites/brand.svg#cib-coreui"></use>
        </svg> -->
        <img class="c-sidebar-brand-minimized" width="38" height="46" src="../bahagia/mini_logo.png" alt="">
        <!-- <svg class="c-sidebar-brand-minimized" width="46" height="46" alt="CoreUI Logo">
          <use xlink:href="../coreui/sprites/brand.svg#cib-coreui"></use>
        </svg> -->
      </div>

      <ul class="c-sidebar-nav ps ps--active-y">

      <li class="c-sidebar-nav-item active">
        <a class="c-sidebar-nav-link" href="index.php">
          <svg class="c-sidebar-nav-icon">
            <use xlink:href="../coreui/sprites/free.svg#cil-home"></use>
          </svg> My Locations
        </a>
      </li>

      <li class="c-sidebar-nav-title">Options</li>

      <li class="c-sidebar-nav-item">
        <a class="c-sidebar-nav-link" href="create_location.php">
          <svg class="c-sidebar-nav-icon">
            <use xlink:href="../coreui/sprites/free.svg#cil-location-pin"></use>
          </svg> Create New Location
        </a>
      </li>


      <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
        <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
      </div>

      <div class="ps__rail-y" style="top: 0px; height: 551px; right: 0px;">
        <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 234px;"></div>
      </div>
    </ul>

    <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent" data-class="c-sidebar-unfoldable"></button>

  </div>

  <div class="c-wrapper">
    <header class="c-header c-header-light c-header-fixed">

      <button class="c-header-toggler c-class-toggler d-lg-none mfe-auto" type="button" data-target="#sidebar" data-class="c-sidebar-show">
        <svg class="c-icon c-icon-lg">

          <use xlink:href="../coreui/sprites/free.svg#cil-menu"></use>
        </svg>
      </button>

      <a class="c-header-brand d-lg-none c-header-brand-sm-up-center" href="#">
        <img id='toggle_brand' class="c-sidebar-brand-full" width="118" height="46" src="../bahagia/bahagia2.png" >
        <!-- <svg width="118" height="46" alt="CoreUI Logo" style="fill:currentColor;">
          <use xlink:href="../coreui/sprites/brand.svg#cib-coreui"></use>
        </svg> -->
      </a>

      <button class="c-header-toggler c-class-toggler mfs-3 d-md-down-none" type="button" data-target="#sidebar" data-class="c-sidebar-lg-show" responsive="true">
        <svg class="c-icon c-icon-lg">
          <use xlink:href="../coreui/sprites/free.svg#cil-menu"></use>
        </svg>
      </button>

      <ul class="c-header-nav mfs-auto">
        <li class="c-header-nav-item px-3 c-d-legacy-none">
          <button class="c-header-nav-btn" type="button" id="header-tooltip" title="Toggle Light/Dark Mode">
            <svg class="c-icon c-d-dark-none">
              <use xlink:href="../coreui/sprites/free.svg#cil-moon"></use>
            </svg>

            <svg class="c-icon c-d-default-none">
              <use xlink:href="../coreui/sprites/free.svg#cil-sun"></use>
            </svg>
          </button>

        </li>
      </ul>
      <div class="d-none d-sm-inline-flex c-header-nav mt-1">
        <h6><?php echo $_SESSION['full_name']; ?></h6>
      </div>
      <ul class="c-header-nav mr-2">
        <li class="c-header-nav-item dropdown">
          <a class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            <div class="c-avatar">
              <img class="c-avatar-img" src="../assets/img/avatars/male_default.jpg" alt="user@email.com">
            </div>
          </a>

          <div class="dropdown-menu dropdown-menu-right pt-0">

          <div class="dropdown-header bg-light py-2"><strong>Settings</strong></div>

            <a class="dropdown-item" href="../logout.php">
              <svg class="c-icon mfe-2">
                <use xlink:href="../coreui/sprites/free.svg#cil-account-logout"></use>
              </svg> Logout
            </a>
          </div>
        </li>
      </ul>

      <!-- <div class="c-subheader justify-content-between px-3">

        <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
          <li class="breadcrumb-item"> <a href="index.php" style="color:grey;">Dashboard</a> </li>

        </ol>

      </div> -->
    </header>
    <div class="c-body">
      <main class="c-main">
        <div class="container-fluid">
          <div class="fade-in">
            <div class="row">
              <div class="col-md-12">
                <div class="card">

                  <div class="card-header">
                    <div class="row">
                      <h6 class="col-10 col-xl-11">Recent locations</h6>
                    </div>
                  </div>

                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-12">



                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
    <footer class="c-footer">
      <div><a href="https://coreui.io">CoreUI</a> © 2020 creativeLabs.</div>
      <div class="mfs-auto">Powered by&nbsp;<a href="https://coreui.io/pro/">CoreUI Pro</a></div>
    </footer>
  </div>
  <!-- CoreUI and necessary plugins-->
  <script src="../coreui/js/jquery.min.js"></script>
  <script src="../coreui/js/coreui.bundle.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function () {
        $('#header-tooltip').on("click",function () {
          var state = $("#body").attr("class");
          var classes = state.split(" ");
          var isDark = false;

          for (var i = 0; i < classes.length; i++) {
            if (classes[i] == "c-dark-theme") {
              isDark = true;
            }
          }

          if (isDark) {
            $('#body').attr("class","c-app flex-row align-items-center");
            $('#toggle_brand').attr("src","../bahagia/bahagia1.png");
            $('#print-btn').css("color","black");
          }else{
            $('#body').attr("class","c-app flex-row align-items-center c-dark-theme");
            $('#toggle_brand').attr("src","../bahagia/bahagia2.png");
            $('#print-btn').css("color","white");
          }
        });
    });
  </script>
  </body>
</html>
