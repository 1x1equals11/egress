<?php
  require 'database/db_connect.php';
  session_start();

  $error = 3; //default
  $error_msg = "";
  $success_msg = "";

  if (isset($_POST['submit'])) {
    $full_name = mysqli_real_escape_string($con, $_POST['full_name']);
    $user_name = mysqli_real_escape_string($con, $_POST['user_name']);
    $password = mysqli_real_escape_string($con, $_POST['password']);

    $password = password_hash($password,PASSWORD_BCRYPT);

    $query = "INSERT INTO account
              (full_name, user_name, password) VALUES
              ('$full_name','$user_name','$password')";

    if (mysqli_query($con,$query)) {
      $error = 1;
      $success_msg = "Successfully created an account.";
    }else{
      $error = 0;
      $error_msg = "Something went wrong. Please contact system administrator.";
    }
  }

 ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Register</title>

    <!-- Main styles for this application-->
    <link href="coreui/css/coreui2.css" rel="stylesheet">

  </head>

  <body class="c-app c-dark-theme flex-row align-items-center">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-6">
          <span style="<?php echo ($error != 3 ? "display:block;" : "display:none;"); ?>">
            <div class="alert <?php echo ( !empty($error_msg) ? "alert-danger": "alert-success" ); ?> text-center" role="alert">
              <?php echo ( !empty($error_msg) ? $error_msg : $success_msg ); ?>
            </div>
          </span>

          <script type="text/javascript">
            var error = <?php echo $error; ?>;

            if (error != 0) {
              setTimeout(function(){
                window.location.href = '/ble/';
              }, 3500);
            }
          </script>
          <div class="card-group">
            <div class="card p-2">
              <!-- was-validated -->
              <div class="card-body ">

                <h1>Sign up</h1>
                <p class="text-muted">Create your account to start localizing your own floor plan</p>

                <form method="post">

                  <div class="input-group mb-2">
                    <input class="form-control" type="text" name="full_name" placeholder="Full Name" style="text-transform:capitalize;" autofocus>
                  </div>

                  <div class="input-group mb-2">
                    <input class="form-control" type="text" name="user_name" placeholder="Username">
                  </div>

                  <div class="input-group mb-2">
                    <input class="form-control" type="password" name="password" placeholder="Password" >
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-6">
                      <button type="submit" class="btn btn-primary px-4" name="submit" type="button">Submit</button>
                    </div>
                    <div class="col-6 text-right">
                      <a class="btn btn-link px-0" href="index.php">Sign in</a>
                    </div>
                  </div>

                </form>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
    <!-- CoreUI and necessary plugins-->
    <script src="coreui/js/coreui.bundle.min.js"></script>

  </body>
</html>
